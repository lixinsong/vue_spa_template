

export default [
    {
        path: '/',
        name: 'home',
        component: () => import(/* webpackChunkName: "home" */ '@containers/views/home/index.vue')
    },
]