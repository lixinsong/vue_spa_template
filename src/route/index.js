import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

let routes = [];
/**
 *  自动扫描子模块路由并导入
 */
const routerContext = require.context('./', true, /index\.js$/);
routerContext.keys().forEach(route => {
    //如果是根目录的index.js,不处理
    if(route.startsWith('./index')){
        return;
    }
    const routeModule = routerContext(route);
    /**
     * 兼容 import export 和 require module.export 两种规范
     */
    routes = [...routes,...(routeModule.default || routeModule)]
})

export default new Router({
    mode: 'history',
    base: "",
    routes: routes
})
