import Vue from 'vue';
import App from '@containers/views/App/index.vue';
import router from '@route/index';
import store from '@store/index';
import '@services/index';
import "./components/index";

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
