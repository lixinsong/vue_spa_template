const path = require('path');
function resolve (dir) {
    return path.join(__dirname, dir)
}
module.exports = {
    lintOnSave: true,
    chainWebpack: (config)=>{
        //这里是调用该文件的别名
        config.resolve.alias
            .set('@', resolve('src'))
            .set('@assets',resolve('src/assets'))
            .set('@route',resolve('src/route'))
            .set('@components',resolve('src/components'))
            .set('@containers',resolve('src/containers'))
            .set('@store',resolve('src/store'))
            .set('@services',resolve('src/services'))
            .set('@utils',resolve('src/utils'))
        // 这里是对环境的配置，不同环境对应不同的BASE_URL，以便axios的请求地址不同
        config.plugin('define').tap(args => {
                args[0]['process.env'].BASE_URL = JSON.stringify(process.env.BASE_URL)
            return args
        })
    }
}